# AutoScripts [![NPM Version](https://img.shields.io/npm/v/wechaty?color=brightgreen)](https://www.npmjs.com/package/wechaty) [![NPM](https://github.com/wechaty/wechaty/workflows/NPM/badge.svg)](https://github.com/wechaty/wechaty/actions?query=workflow%3ANPM) 


[![GitHub stars](https://img.shields.io/github/stars/HenryMartinMetaverse/autoscripts.svg?label=github%20stars)](https://github.com/HenryMartinMetaverse/autoscripts)
[![ES Modules](https://img.shields.io/badge/ES-Modules-orange)](https://github.com/HenryMartinMetaverse/autoscripts/issues)
[![WeChat](https://img.shields.io/badge/--07C160?logo=wechat&logoColor=white)](https://wechaty.js.org/docs/puppet-providers/wechat)
[![Telegram Wechaty Channel](https://img.shields.io/badge/chat-on%20telegram-blue)](https://t.me/wechaty)

## Introduce


[农民世界](https://play.farmersworld.io/)自动剧本，自动采矿和维修，自动养鸡，养牛，种地，自动build地块、牛棚、鸡舍，自动补充能量。自动赚钱。

基于后台技术开发，运行效率高，稳定不死机，单台服务器可以运行50多个账号。当线路离线时自动重启。比前端脚本，QuickMacro, AutoHotkey多15%-20%的收入。

跨平台, 支持 Windows, Linux, MacOS.

:beetle: <https://play.farmersworld.io/>  
:octocat: <https://gitee.com/romejiang/autoscripts>  

## 因为农民世界开启了2FA安全加密机制，所以我们从2021年12月23日开始提供打包版程序，使用更简单一键启动。

### win10/win11 版本下载地址

百度云盘

链接：https://pan.baidu.com/s/1GUky5CYiOxenSQGovk0xJw?pwd=h6cc 
提取码：h6cc

坚果云（速度快，但要注册）

https://www.jianguoyun.com/p/DcYPU8kQlfOVChjHz6oE

## 一键启动

![一键启动的界面](WechatIMG921.png)

## 源码安装和使用教程


[![node](https://img.shields.io/node/v/wechaty.svg?maxAge=604800)](https://nodejs.org/)

### ** 注意！！以下是源码安装使用教程，打包版在上面，下载后一键自动使用。** 


#### [ 视频教程 bilibili ，点击播放 ](https://www.bilibili.com/video/BV14L4y1i7pS/)


#### [ 视频教程 youtube ，点击播放 ](https://www.youtube.com/watch?v=ojy8CEl2eXw)


###  Windows 安装运行环境


运行前需要下载安装软件环境，因为是源码运行，所以需要开发环境。

1. 在这里下载 git 程序并安装 https://git-scm.com/download/win

2. 在这里下载 nodejs 并安装 https://nodejs.org/en/

3. 然后点开始，输入 cmd ，在cmd文件上右键，选择用管理员权限运行，在新窗口里输入 npm install --global --production windows-build-tools --verbose

### 开始使用
   
1. cd \

2. 下载源码 git clone https://gitee.com/romejiang/autoscripts.git

3. 进入目录，安装 npm包， cd autoscripts && npm i --verbose

4. 运行设置脚本，node setup ，首次运行需要输入账号地址，并手动登录钱包
   
5. 运行自动脚本，node index , 需要输入地址。

6. 如果需要多账号多开，就在重复4到5的上述动作 。



#### [ 视频教程 bilibili ，点击播放 ](https://www.bilibili.com/video/BV14L4y1i7pS/)


#### [ 视频教程 youtube ，点击播放 ](https://www.youtube.com/watch?v=ojy8CEl2eXw)


## 常见问题

1. npm i 如果报错了，重新执行 npm i 即可

2. 如果npm i 或者安装 windows-build-tools 时总是报错。执行下面的命令，换用国内的npm源。然后在重新执行 npm i

```
npm i -g nrm
nrm use cnpm
``` 

3. 如果代码更新了，执行一下代码

```
git fetch --all
git reset --hard origin/main
git pull

```

4. 高级功能: 命令行支持直接输入钱包账号，类似这样： node index xxxxx.wam

5. 高级功能: 多开的情况下，在命令行后面加入数字，可以让窗口错开显示，比如：node index xxxxx.wam 1

6. 高级功能: 允许只运行部分模块。比如只挖矿和养牛就是14，

``` node index xxxxx.wam 1 14```

只养鸡就是2

``` node index xxxxx.wam 1 2```


只挖矿，种地，同时build牛棚就是 135


``` node index xxxxx.wam 1 135```


默认是所有都开启

```
1 = 挖矿mined
2 = 养鸡chicken
3 = 种地farming 
4 = 养牛cow
5 = 建造(包括：地块，牛棚，鸡舍) build (FARM PLOT/COOP/COWSHED)
6 = 自动售卖
```

## Support

技术支持微信号： thegamefi


技术支持微信群

<img src="https://images.gitee.com/uploads/images/2021/1119/162750_bff68ef1_6103.jpeg" width = "300" height = "360" alt="技术支持微信群" align=center />





## Copyright & License

- Code & Docs © 2021-now
- Code released under the Apache-2.0 License
- Docs released under Creative Commons
